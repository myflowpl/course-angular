## Tworzymy moduł shared 
```
ng generate module shared
```

## Tworzymy komponenty
menu, footer, layout
```
ng g c shared/components/menu
ng g c shared/components/footer
```
obserwujemy wygenerowane pliki

analizujemy jak automatycznie CLI uaktualnia NgModules()


## Użyjmy naszego modułu w aplikacji
importujemy `SharedModule` w `app.module.ts`

uaktualniamy `app.component.html` zastępując wygenerowany kod naszymi komponentami

```html
<app-menu></app-menu>
<router-outlet></router-outlet>
<app-footer></app-footer>
```
jeszcze powinniśmy zobaczyć error

musimy wyeksportować nasze komponenty z SharedModule



## przerabiamy zagadnienia
- router outlet
- text interpolation
- template expresions
- viewEncapsulation, ShadowDom, host, host-context, ng-deep
- change detection
- property binding
- ngStyle
- Click handling, hidden attribute
- *ngIf
- ng-template, ngIf
- *ngIf ngIfElse
- ngIf ngIfThen ngIfElse

## commitujemy zmiany
message: shared module + components 
