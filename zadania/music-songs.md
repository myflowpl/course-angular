
Songs
1. Tworzymy nowe komponenty
    1. ng g c music/song-edit-form
    2. ng g c music/songs
    3. ng g c music/song-view
    4. ng g c music/song-add
    5. ng g c music/song-edit
2. tworzymy nowy serwis
    1. ng g s music/song
3. dodajemy do menu głównego nowy routing
    1. edytujemy app.component.html
4. konfigurujemy routing
    1. edytujemy plik music-routing.module.ts
    2. /music/songs – lista piosenek
    3. /music/songs/:id – view/edit song
    4. /music/songs/add – dodawanie nowej piosenki
5. dodaj mockowe przyciski żeby zaprezentować działanie routingu po komponentach
    
Playlists

1. nowa zakładka w głównym menu “playlists” w pliku app.component.html
2. nowy routing w module music.module.ts, 
3. nowy komponent PlaylistsComponent w module music
    1. ng g c music/playlists
4. nowy serwis PlaylistService 
    1. ng g s music/playlist
5. wyświetlenie listy playlists
    1. posiada layout z dwiema kolumnami
        1. w pierwszej klikalna lista z inputem na dole do dodawania nowej playlisty
        2. w drugiej kolumnie ładujemy listęp piosenek po kliknięciu playlisty
    2. PlaylistComponent pobiera dane z PlaylistService
6. wyświetlenie piosenek każdej z playlisty jako routing zagnieżdzony
    1. routing dodajemy jako children do routingu playlists
    2. tworzymy PlaylistSongsComponent
7. usuwanie piosenki z playlisty
    1. Tworzymy
8. tworzenie nowej playlisty
    1. dodajemy form na dole listy
9. edycja nazwy playlisty
    1. Tworzymy komponent PlaylistItemComponent
        1. ng g c music/playlist-item
    2. 