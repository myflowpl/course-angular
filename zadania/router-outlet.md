


## home component
generujemy komponent
```
ng g c shared/components/home
```
dodajemy routing dla strony głównej w pliku `src/app/app-routing.module.ts`
```typescript
import { HomeComponent } from './shared/components/home/home.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent
}];
```


## przerabiamy zagadnienia
- router outlet
- text interpolation
- template expresions
- viewEncapsulation, ShadowDom, host, host-context, ng-deep
- change detection
- property binding
- ngStyle
- Click handling, hidden attribute
- *ngIf
- ng-template, ngIf
- *ngIf ngIfElse
- ngIf ngIfThen ngIfElse

## commitujemy zmiany
message: shared module + components + bootstrap



## tworzymy nasz menu komponent
użyjemy komponentu navbar z biblioteki bootstrap: `https://getbootstrap.com/docs/4.0/components/navbar/`
w `menu.component.html`
```html
<nav class="navbar fixed-top navbar-expand navbar-light bg-light">
  <a class="navbar-brand" href="#">AngularApp</a>
  <ul class="navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="#">Home</a>
    </li>
  </ul>
  <ul class="navbar-nav ml-auto">
    <li class="nav">
      <button class="btn btn-default nav-link">Login</button>
    </li>
  </ul>

</nav>
```