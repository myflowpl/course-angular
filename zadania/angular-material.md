
## dodajemy bibliotekę `@angular/material` urzywając `ng add <package>`
```
ng add @angular/material
```
- jako theme wybieramy pierwszą pozycję Indigo/Pink
- zgadzamy się na zainstalowanie HammerJS
- zgadzamy się na animacje

analizujemy zmiany w plikach:
- src/package.json
- src/main.ts
- src/app/app.module.ts
- angular.json
- src/index.html
- src/styles.scss

## importujemy moduły

```typescript
...

```