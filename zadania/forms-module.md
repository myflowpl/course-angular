## tworzymy formularz w `search.component.html`
```html
<form class="form-inline">
  <div class="form-group mx-sm-3">
    <input name="query" type="text" class="form-control" placeholder="Szukaj na youtube">
  </div>
  <button type="submit" class="btn btn-primary">Szukaj</button>
</form>
<p>
  Szukam: {{query}}
</p>
```
w pliku `search.component.ts` dodajemy propertis `query: string;`

## dyrektywa ngModel
do inputa dodajemy dyrektywę `[(ngModel)]="query"`

powinniśmy zobaczyć błąd w konsoli, ngModel jest nieznany

## dodajemy FormsModule
w pliku `videos.module.ts` dodajemy `FormsModule` w sekcji `import`

teraz powinniśmy widzieć działający formularz

## event ngSubmit
w pliku `search.component.html` podpinamy się pod event submit
```html
<form class="form-inline" (ngSubmit)="onSubmit($event)">
```
w pliku `search.component.ts` tworzymy handler
```typescript
onSubmit() {
  console.log('SUBMIT', this.query);
}
```
## dodajemy prostą walidację
do elementu `input` dodajemy atrybut `required`

do elementu `form` dodajemy referencję ` #searchForm="ngForm"`

do buttona submit dodajemy bindowanie do propertisa disabled
```html
<button type="submit" class="btn btn-primary" [disabled]="!searchForm.form.valid">Szukaj</button>
```
## ngModelChange

wyjmujemy banana from the box

## commint
FormsModule

## do nakuki
- lazy loaded modules
- router links
- router active links
- submenu routing
- routing redirections
- FormsModule
- ngSubmit
- form validation
- ngModel 2-way data binding
- ngModel ngModelChange
