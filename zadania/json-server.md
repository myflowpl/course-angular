## mockowy serwer
instalacja
```
npm i json-server --save-dev
```

package.json
```
"api": "json-server db.json"
```
kopiujemy plik `db.json` do glownego katalogu:

https://bitbucket.org/myflowpl/szkolenie-angular/src/init/api/db.json

uruchamiamy
```
npm run api
```

## dodajemy konfigurację
w environements
```
imagesBaseUrl: 'http://www.songnotes.cc/images/artists/',
baseUrl: 'http://localhost:3000'
```
providery w `app.module.ts`
```
{
  provide: BASE_URL,
  useValue: environment.baseUrl
},
{
  provide: IMAGES_BASE_URL,
  useValue: environment.imagesBaseUrl
}
```


## przydatne endpointy

lista piosenek
``` 
http://localhost:3000/songs?_expand=artist 
```

lista playlists songs
``` 
http://localhost:3000/playlistSongs?_expand=song&_expand=playlist 
```

piosenki playlisty
```
http://localhost:3000/playlistSongs?_expand=song&_expand=playlist&playlistId=2
```

## modele

```typescript
export interface Coords {
  lat: number;
  lng: number;
}
export interface Artist {
  id: number;
  name: string;
  img: string;
  location?: Coords;
}
export interface Playlist {
  id: number;
  name: string;
}

export interface PlaylistSongs {
  id: number;
  playlistId: number;
  songId: number;
  song?: Song;
  playlist?: Playlist;
}

export interface Song {
  id: number;
  title: string;
  year: string;
  artistId: number;
  webUrl: string;
  genders?: string[];
  favorite?: boolean;
}

export interface User {
  id: number;
  name: string;
  password: string;
  role?: string;
}
```