
### Reactive Forms `song-form.component`

1. plik `song-form.component.ts`
2. komunikacja `@Input @Output` ze światem zewnętrznym
    1. input – atrybut `song` - edycja
    2. output – event `save`
3. użyj reactive form
4. pola formularza
    1. `title` - text
    2. `year` – text
    3. `favorite` - checkbox
    4. `genders` – array, dynamiczne pole tekstowe + przycisk dodaj gender
    5. przycisk `submit`
5. dodaj walidację na pola
6. dodaj walidację na max liczbę genders na 3

html
```html
<form (ngSubmit)="onSubmit()" [formGroup]="songForm">
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" formControlName="title">
    <small id="emailHelp" class="form-text text-muted">The cool song name...</small>
  </div>
  <div class="form-group">
    <label for="year">Year</label>
    <input type="number" class="form-control" id="year" formControlName="year">
  </div>
  <label>Genders</label>

  <div formArrayName="genders">
    <div *ngFor="let gender of genders.controls; let i=index">
      <div class="form-group">
        <input type="text" class="form-control" [formControlName]="i" />
        <button type="button" class="btn btn-default" (click)="removeGender(i)">X</button>

      </div>
    </div>
    <button type="button" class="btn btn-default" (click)="addGender()">add gender</button>

  </div>

  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="favorite" formControlName="favorite">
    <label class="form-check-label" for="favorite">my favorite</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  <button (click)="cancel.next()" type="button" class="btn btn-default">cancel</button>
</form>

```
css
```css
.ng-invalid.ng-touched {
  border-color: red;
}
```

ts
```typescript
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Song } from '../models/artist.model';
import { FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-song-edit-form',
  templateUrl: './song-edit-form.component.html',
  styleUrls: ['./song-edit-form.component.scss']
})
export class SongEditFormComponent implements OnInit {

  @Input() song: Song;
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();

  songForm = this.fb.group({
    title: ['', Validators.required],
    year: ['', Validators.required],
    favorite: [false, Validators.required],
    genders: this.fb.array([])
  });

  get genders() {
    return this.songForm.get('genders') as FormArray;
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    if (this.song) {
      if (this.song.genders) {
        this.song.genders.forEach(v => this.genders.push(this.fb.control('')));
      }
      this.songForm.patchValue(this.song);
    }
  }

  onSubmit() {
    console.log('SUBMIT', this.songForm.valid, this.songForm.value);

    this.save.next(this.songForm.value);

    // if (this.songForm.valid) {
    // this.save.next(this.songForm.value);
    // }
  }

  addGender() {
    this.genders.push(this.fb.control(''));
  }

  removeGender(index: number) {
    this.genders.removeAt(index);
  }
}

```