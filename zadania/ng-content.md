## dyrektywy z projekcją


artist profile
```html
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="{{artist.img | imageBaseUrl}}" alt="{{artist.name}}" onerror="this.onerror=null;this.src='https://placeimg.com/200/200/animals';">
  <div class="card-body">
    <h5 class="card-title">{{artist.name}}</h5>
    <p class="card-text">
      <!-- Location: <br> {{artist.location.lat}}, {{artist.location.lng}} -->
    </p>
     <ng-content></ng-content>
  </div>
</div>

```
## youtube embeded video
umieszczanie video
```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/1KT2asqA1J8" frameborder="0" allowfullscreen></iframe>
```
sanitize URL 

```typescript
import { DomSanitizer } from '@angular/platform-browser';
```
constructor
```typescript
constructor(videoURL: string, private _sanitizer: DomSanitizer){
   this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(videoURL);
}
```
bindowanie safeUrl do iframe.
```html
<iframe [src]='safeURL' frameborder="0" allowfullscreen></iframe>
```

## pipes
imageBaseUrl
```typescript
import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IMAGES_BASE_URL } from 'src/app/app-config';

@Pipe({
  name: 'imageBaseUrl'
})
export class ImageBaseUrlPipe implements PipeTransform {

  constructor( @Inject(IMAGES_BASE_URL) private imageBaseUrl: string) {}
  transform(value: any, args?: any): any {
    return this.imageBaseUrl + value;
  }

}
```