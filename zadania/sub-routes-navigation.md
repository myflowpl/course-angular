```
ng g c videos/videos-menu
```
dodajemy linki
```html
<p>
  <button [routerLink]="['../search']" routerLinkActive="hidden" class="btn btn-primary">Search</button>
  <button [routerLink]="['../list']" routerLinkActive="hidden" class="btn btn-primary">List</button>
</p>
```
css
```css
p {
  text-align: center
}
.hidden {
  display: none;
}
```
tworzymy liste dodanych filmow

```
ng g c videos/list
```

uaktualniamy `videos-routing.module.ts` i didajemy routing dla list
```typescript
{
  path: 'list',
  component: ListComponent
}
```

umieszczamy menu w `list.component.html`

## refactor `search.service.ts`
```typescript
searchYoutube(query: string): Observable<YoutubeResponse> {

    const endpoint = `https://www.googleapis.com/youtube/v3/search`;

    return this.http.get<YoutubeResponse>(endpoint, {params: {
        q: query,
        part: 'snippet',
        key: this.key
    }});
}
```
## tworzymy models
```
src/app/models/index.ts
src/app/models/models.ts

```

## tworzymy serwis videos
```
ng g s videos/videos
```

```typescript
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { startWith, merge, switchMap } from 'rxjs/operators';
import { Video } from '../models';
import { BASE_URL } from '../shared/tokens';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  reload$ = new Subject();

  constructor(private http: HttpClient, @Inject(BASE_URL) private baseUrl: string) { }

  getVideos(): Observable<Video[]> {

    return Observable.create().pipe(
      startWith(1),
      merge(this.reload$),
      switchMap((v) => {
        return this.http.get<Video[]>(this.baseUrl + '/videos');
      })
    );
  }

  getSongById(id: string | number): Observable<Video> {
    return this.http.get<Video>(this.baseUrl + '/videos/' + id);
  }


}

```
