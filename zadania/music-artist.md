
## tworzymy modul music
```
ng g m music --routing
```

## smart componenty
```
ng g c music/artist
ng g c music/artist-view
ng g c music/artist-edit
```
## simple componenty
```
ng g c music/components/artist-profile
```
## serwis
```
ng g s music/services/artist
```
## routing
dodajemy do menu

dodajemy do glownego routingu

dodajemy routing w module  `music-routing.module.ts`
```typescript
{
  path: 'artists',
  component: ArtistComponent,
  children: [
    {
      path: 'view/:id',
      component: ArtistViewComponent
    },
    {
      path: 'edit/:id',
      component: ArtistEditComponent
    }
  ]
}

```

## artist component
html
```html
<div class="container">
  <form>
    <input class="form-controll" name="name" [(ngModel)]="query" />
    <button (click)="search()">search</button>
  </form>

  <div class="row">
    <div class="col col-lg-2">
      <div *ngIf="artists$ | async as artists; else loading" class="card-group">
        <div *ngFor="let artist of artists">
          <app-artist-profile [artist]="artist">
            <a class="btn btn-primary" [routerLink]="['artist', artist.id]">Show Songs</a>
          </app-artist-profile>
        </div>
      </div>
      <ng-template #loading let-user>Waiting...</ng-template>
    </div>
    <div class="col-sm">
      <router-outlet></router-outlet>
    </div>
  </div>
</div>
```
artist component ts

```typescript
import { Component, OnInit } from '@angular/core';
import { MusicService } from '../music.service';
import { Artist } from '../models/artist.model';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public query = '';

  public artists$: Observable<Artist[]>;

  constructor(private musicService: MusicService) { }

  ngOnInit() {
    this.artists$ = this.musicService.getArtists().pipe(
      delay(500)
    );
  }

  search() {
    console.log('E', this.query);
    if (!this.query) { return; }

    this.artists$ = this.musicService.searchArtist(this.query).pipe(
      delay(500)
    );
  }

}
```

## artist service
```typescript
import { Injectable, InjectionToken, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { share, shareReplay } from 'rxjs/operators';
import { Artist } from './models/artist.model';
import { environment } from 'src/environments/environment';
import { BASE_URL } from '../app-config';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(private http: HttpClient, @Inject(BASE_URL) private baseUrl: string) { }

  getArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.baseUrl + '/artists');
  }

  getArtist(id: string): Observable<Artist> {
    return this.http.get<Artist>(this.baseUrl + '/artists/' + id + '?_embed=songs');
  }

  searchArtist(query: string): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.baseUrl + '/artists?q=' + query);
  }

  updateArtist(id: string, data: Partial<Artist>): Observable<any> {
    return this.http.patch<any>(this.baseUrl + '/artists/' + id, data);
  }
}
```

## artist view component
```typescript
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, delay, merge, map } from 'rxjs/operators';
import { MusicService } from '../music.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {

  artist$;
  constructor(
    private route: ActivatedRoute,
    private musicService: MusicService
  ) { }

  ngOnInit() {
    this.artist$ = this.route.paramMap.pipe(
      map(paramsMap => paramsMap.get('id')),
      // merge(this.onBtnClick$),
      switchMap((id) => {
        return this.artistsService.getArtist(id).pipe(delay(1000));
      })
    );
  }

}
```
artist vie component html
```html

<ng-container *ngIf="artist$ | async as artist; else elseTemplate">

  <app-artist-profile [artist]="artist">
    <a class="btn btn-primary" [routerLink]="['../../artist-edit', artist.id]">edit</a>
  </app-artist-profile>

  <app-map [artist]="artist"></app-map>

</ng-container>

<ng-template #elseTemplate>
  Loading...
</ng-template>

```

## artist profile
```html
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="{{artist.img | imageBaseUrl}}" alt="{{artist.name}}" onerror="this.onerror=null;this.src='https://placeimg.com/200/200/animals';">
  <div class="card-body">
    <h5 class="card-title">{{artist.name}}</h5>
    <p class="card-text">
      <!-- Location: <br> {{artist.location.lat}}, {{artist.location.lng}} -->
    </p>
     <ng-content></ng-content>
  </div>
</div>

```


## artist edit component

```html
<form [formGroup]="artistForm">

  <input type="hidden" formControlName="id">

  <label>
    Name:
    <input type="text" formControlName="name">
  </label>

  <label>
    Image:
    <input type="text" formControlName="img">
  </label>

  <button (click)="save()">SAVE</button>
</form>
```

```typescript
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MusicService } from '../music.service';
import { switchMap, tap, takeUntil } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-artist-edit',
  templateUrl: './artist-edit.component.html',
  styleUrls: ['./artist-edit.component.scss']
})
export class ArtistEditComponent implements OnInit, OnDestroy {

  destroy$ = new Subject();
  artistForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private musicService: MusicService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      takeUntil(this.destroy$),
      switchMap((paramsMap) => {
        return this.musicService.getArtist(paramsMap.get('id'));
      }),
    ).subscribe((artist) => {
      this.artistForm.patchValue(artist);
    });

    this.artistForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      img: new FormControl(''),
    });
  }

  save() {
    const data = this.artistForm.getRawValue();
    console.log('DATA', data);
    this.musicService.updateArtist(data.id, data).subscribe(res => {
      console.log('RES', res);
      this.router.navigate(['music', 'search', 'artist', data.id]);
    });
  }

  ngOnDestroy() {
    this.destroy$.next(1);
  }

}
```

