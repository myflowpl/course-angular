## tworzymy nowy moduł videos
```
ng g m videos --routing
```
## dodajemy go w routingu aplikacji
tym razem jako lazy loaded module w pliku `app-routing.module.ts` dodajemy kolejny wpis w tablicy `routes`
```typescript
{
  path: 'videos',
  loadChildren: './videos/videos.module#VideosModule'
}
```
## tworzymy w menu link do tego modułu
`menu.component.html`
```html
<a class="navbar-brand" routerLink="">AngularApp</a>
<ul class="navbar-nav">
  <li class="nav-item" routerLinkActive="active"  [routerLinkActiveOptions]="{exact: true}">
    <a class="nav-link" routerLink="">Home</a>
  </li>
  <li class="nav-item" routerLinkActive="active">
    <a class="nav-link" [routerLink]="['videos']">videos</a>
  </li>
</ul>
```

## tworzymy `search.component` w module videos
```
ng g c videos/search
```

## edytujemy routing
```typescript
import { SearchComponent } from './search/search.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'search'
}, {
  path: 'search',
  component: SearchComponent
}];
```
testujemy czy wszystko pojawia się search module

## commitujemy
feature module routing

## do nakuki
- lazy loaded modules
- router links
- router active links
- submenu routing
- routing redirections
