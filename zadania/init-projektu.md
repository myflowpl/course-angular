## Instalacja Angular CLI
```
npm i -g @angular/cli@latest
```
## tworzymy nowy projekt
```
ng new angular-project
```
- wybieramy `routing` na `TAK`
- wybieramy `SCSS` jako system styli
- zgadzamy się na `Hammer.js`

## otiweramy projekt w Visual Studio Code
```
code angular-project
```

## uruchamiamy serwer developerski
najlepiej użyc konsoli systemowej, a nie wbudowanej w VS Code
```
cd angular-project
npm run start
```

## commitujemy zmiany
- przełączamy się na widok kontroli wersji, w lewym menu klikamy 3 ikonkę od góry
- dodajemy wszystkie pliki do kommita
- jako message podajemy: init projektu z @angular/cli
- klikamy ikonkę zatwierdź (lub ctrl+enter)

## analizujemy elementy projektu
- angular.json
- package.json, package-lock.json, node_modules
- tslint.json
- tsconfig.json
- src
- e2e
