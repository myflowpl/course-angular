
## generujemy potrzebne elementy
```
ng g c shared/dialogs/auth-dialog --export
ng g s shared/services/auth
ng g s shared/services/user
ng g d shared/directives/auth
```

## auth dialog component
html
```html
<form [formGroup]="loginForm" (ngSubmit)="onLogin()">

  <h1 mat-dialog-title>Login</h1>
  <div mat-dialog-content>
    <p>User name</p>
    <mat-form-field>
      <input matInput formControlName="username" />
    </mat-form-field>
    <p>Password</p>
    <mat-form-field>
      <input matInput type="password" formControlName="password" />
    </mat-form-field>
  </div>
  <div mat-dialog-actions>
    <button mat-button (click)="onCancel()">cancel</button>
    <button mat-button type="submit">Login</button>
  </div>
</form>

```
ts
```typescript
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-auth-dialog',
  templateUrl: './auth-dialog.component.html',
  styleUrls: ['./auth-dialog.component.scss']
})
export class AuthDialogComponent implements OnInit {

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  user: any;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialogRef<AuthDialogComponent>,
    private userService: UserService
  ) { }

  ngOnInit() {}

  onCancel() {
    this.dialog.close(null);
  }

  onLogin() {
    if (this.loginForm.valid) {
      const data = this.loginForm.value;
      this.userService.login(data.username, data.password).subscribe(user => {
        console.log('SUCCESS', user);
        this.dialog.close(user);
      }, (err) => {
        console.log('ERR', err);
      });
    }
  }
}

```

## auth service
```typescript
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from 'src/app/models';
import { UserService } from './user.service';
import { switchMap, share, take } from 'rxjs/operators';
import { AuthDialogComponent } from '../dialogs/auth-dialog/auth-dialog.component';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private userService: UserService, private dialog: MatDialog) { }

  public loginDialog$ = this.userService.user$.pipe(
    switchMap(user => {

      if (!user) {
        const dialogRef = this.dialog.open(AuthDialogComponent, {
          height: '400px',
          width: '600px',
        });

        return dialogRef.afterClosed();
      } else {
        return of(user);
      }
    }),
    share(),
    take(1)
  );
}


```

## auth directive
```typescript
import { Directive, HostListener } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Directive({
  selector: '[appAuth]'
})
export class AuthDirective {

  constructor(private authService: AuthService) { }

  @HostListener('click')
  onclick() {
    this.authService.loginDialog$.subscribe();
  }
}

```


## user service
v1
```typescript
import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, of, throwError, Subject, Observable, interval } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { switchMap, tap, startWith, merge, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { BASE_URL } from '../tokens';
import { User } from 'src/app/models';

const sessionDuration = 120;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private onRequest$ = new Subject();
  public refreshButtonClick$ = new Subject();

  private _idleTime$ = new BehaviorSubject(sessionDuration);

  private _user$ = new BehaviorSubject<null | User>(null);

  constructor(
    private dialog: MatDialog,
    private http: HttpClient,
    @Inject(BASE_URL) private baseUrl: string
  ) {

    const userFromStorage = localStorage.getItem('user');
    if (userFromStorage) {
      try {
        this._user$.next(JSON.parse(userFromStorage));
      } catch (error) {
        console.log('ERROR parsowania', error)
      }
    }
    console.log('USER FROM SESSION', userFromStorage);

    this._user$.subscribe(user => {
      console.log('SAVE SESSION', user);
      localStorage.setItem('user', JSON.stringify(user));
    });


    Observable.create().pipe(
      startWith(1),
      merge(this.onRequest$, this.refreshButtonClick$),
      switchMap(() => {
        return interval(1000);
      }),
      map((time: number) => {
        return sessionDuration - time;
      }),
      map((time: number) => {
        return time >= 0 ? time : 0;
      })
    ).subscribe(time => {
      if (!time && this._idleTime$.getValue() !== 0) {
        this.logout();
      }
      this._idleTime$.next(time);
    });
  }

  get idleTime$() {
    return this._idleTime$.asObservable();
  }

  get user$() {
    return this._user$.asObservable();
  }

  onRequest(req) {
    this.onRequest$.next();
  }

  login(username: string, password: string) {
    // TODO to jest kod mockowy do testowego api, na produkcji nie robimy tego GETem ;)
    return this.http.get<User[]>(this.baseUrl + '/users?name=' + username + '&password=' + password).pipe(
      switchMap((users) => {
        if (users.length) {
          return of(users[0]);
        } else {
          return throwError('not found');
        }
      }),
      tap(user => this._user$.next(user))
    );
  }

  logout() {
    this._user$.next(null);
  }

}

```

## elementy menu
```html
<ul class="navbar-nav ml-auto">

    <li class="nav-item">
        <ng-container *ngIf="userService.user$ | async as user; else elseTemplate">
          <button class="nav-link">{{user.name}} <span (click)="userService.logout()">[logout]</span></button>
        </ng-container>
        <ng-template #elseTemplate>
          <button class="nav-link" appAuth>Login</button>
        </ng-template>

      </li>

      <!-- <li class="nav-item" *appIsAdmin="userService.user$ | async" >
        <a class="nav-link" >ADMIN</a>
      </li> -->

      <li class="nav-item">
        <a class="nav-link" *ngIf="userService.idleTime$ | async as time">
          idle time: <b>{{time}}s</b>
          <span *ngIf="time < 3" (click)="userService.refreshButtonClick$.next()">REFRESH</span>
        </a>
      </li>

  </ul>
```


## hasRole directive
```typescript
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

/**
 * Add the template content to the DOM unless the condition is true.
 */
@Directive({ selector: '[appHasRole]'})
export class HasRoleDirective {
  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  @Input() set appHasRole(role: string) {
    if (!role && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (role && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
```