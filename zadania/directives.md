## dyrektywy 
highlight
```typescript
import { Directive, HostBinding, HostListener, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {


  @HostBinding('style.text-decoration')
  decoration = '';

  constructor(private el: ElementRef, private renderer: Renderer) {  }

  @HostListener('mouseover')
  onmouseover() {
    console.log('over');
    this.decoration = 'underline';

    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', 'yellow');
  }

  @HostListener('mouseout')
  onmouseout() {
    console.log('out');
    this.decoration = '';
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', '');
  }

}
```

## dyrektywy strukturalne

myUnless
```typescript
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

/**
 * Add the template content to the DOM unless the condition is true.
 */
@Directive({ selector: '[myUnless]'})
export class UnlessDirective {
  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  @Input() set myUnless(condition: boolean) {
    if (!condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
```
