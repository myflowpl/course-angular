## dodajmy bibliotekę bootstrap
żeby ostyować nasze kompnenty użyjemy frameworka css
```
npm i bootstrap
```
użyjemy pluginu `Search node_modules` w vs code do odszukania plikow css i dodamy je do projektu w `angular.json`
```json
"styles": [
  "./node_modules/bootstrap/dist/css/bootstrap.css",
  "src/styles.scss"
],
```
lecz style nie powinny się wyświetlać poprawnie

każda zmiana w `angular.json` wymaga restartu serwera developerskiego

restartujemy proces w konsoli z `npm run start`