## instalacja ngrx
Instalujemy wymagane paczki jako `dependencies`, flaga `--save` jest już nie wymagana
```
npm install @ngrx/store @ngrx/effects @ngrx/entity @ngrx/router-store @ngrx/store-devtools
```

## instalacja ngrx schematics
Tu dodajemy flagę `--save-dev` jako zależności `devDependencies`
```
npm install @ngrx/schematics --save-dev
ng config cli.defaultCollection @ngrx/schematics
```

## setup ngrx w projekcie
zainstalowalismy schematics i teraz mozemy ich uzyć do setapu appki

### generate root store
```
ng generate store State --root --statePath store/reducers --module app.module.ts
```
dla wersji CLI 7.1.1 należy naprawić błędną ścieżkę do environements
```
import { environment } from '../../environments/environment'; // ŹLE
import { environment } from '../environments/environment'; // DOBRZE
```

### generate root actions
```
ng generate action store/actions/user
```
### generate root reducers
```
ng generate reducer store/reducers/auth --reducers index.ts
```
### generate root effects
```
ng generate effect store/App --group --root --spec false --module app.module
ng generate effect store/User --group --root --spec false --module app.module
ng generate effect store/effects/auth --module app.module --root true
```
### generate component with access to the store
```
ng generate container welcome --state store/reducers/index.ts --stateInterface State
```

## generacja feature
FEATURE
```
ng generate feature store/videos --module app.module.ts --reducers reducers/index.ts --force --group
```

## effects
```typescript
 @Effect() loadCustomers$: Observable<Action> = this.actions$.pipe(
    ofType<customerActions.loadCustomersAction>(types.LOAD_CUSTOMERS),
    mergeMap(() => this.customerService.getCustomers().pipe(
      map(customers => (new customerActions.loadCustomersSuccessAction(customers)))
    ))
  )
```

## selectors

store/spots/spots.reducer.ts
```typescript
export const selectLayerSpots = (state: State) => state.layer_spots;
```

store/index.ts
```typescript
export const selectSpots = (state: State) => state.spots;
export const selectSpotsLayers = createSelector(selectSpots, fromSpots.selectLayerSpots);
export const selectSpotsLayer = function (layer: LayerType) {
  return createSelector(
    selectSpotsLayers,
    (layers) => layers[layer]
  );
};
```

## dodatkowe źródła 
- setup & root store: https://www.intertech.com/Blog/ngrx-tutorial-quickly-adding-ngrx-to-your-angular-6-project/
- actions/reducers/effects: https://www.intertech.com/Blog/ngrx-tutorial-actions-reducers-and-effects/
- selectors: 
    - selectors & components https://www.intertech.com/Blog/ngrx-tutorial-accessing-state-in-the-store/
    - Tod Motto - selectors in depth https://toddmotto.com/ngrx-store-understanding-state-selectors
- add router to state: https://www.intertech.com/Blog/ngrx-tutorial-add-router-info-to-state/
- feature mudules : https://www.intertech.com/Blog/ngrx-tutorial-add-state-to-feature-module/
- 