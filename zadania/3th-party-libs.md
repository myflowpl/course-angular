## instalujemy bibliotekę leaflet
```
npm i leaflet
```

## konfigurujemy assets do `angular.json`
pamietamy o restarcie serwera
```typescript
"assets": [
  "src/favicon.ico",
  "src/assets",
  {
    "glob": "**/*",
    "input": "node_modules/leaflet/dist",
    "output": "assets/leaflet"
  }
]
```
## w `index.html` dodajemy 
```html
<head>
<link href="/assets/leaflet/leaflet.css" rel="stylesheet">
</head>
```
## dodajemy typings
```
npm i @types/leaflet --save-dev
```