## user guard
```
ng g guard shared/guards/user
```

guard
```typescript
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './user.service';
import { take, map } from 'rxjs/operators';
import { LoginService } from './shared/login.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log('GUARD ROLES', next.data);


    return this.loginService.loginDialog().pipe(
      take(1),
      map(user => {
        console.log('USER GUARD', user);

        if (next.data.roles && user && next.data.roles.indexOf(user.role) >= 0) {
          return true;
        } else {
          this.router.navigate(['/error/forbidden']);
          return false;
        }
      }),
      map(u => !!u)
    );
  }
}
```
resolver
```typescript
import { Injectable } from '@angular/core';
import { Artist } from '../models/artist.model';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { MusicService } from '../music.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorResolverService implements Resolve<Artist[]> {

  constructor(private artist: MusicService) { }

  resolve(): Observable<Artist[]> {
    return this.artist.getArtists();
  }
}
```

usage in routing
```typescript
{
  path: 'songs',
  component: SongsComponent,
  children: [{
    path: 'add',
    component: SongAddComponent,
    canActivate: [UserGuard],
    data: {
      roles: ['admin']
    },
    resolve: {
      artists: AuthorResolverService
    }
  }, {
    path: ':id',
    component: SongViewComponent
  }]
}
```